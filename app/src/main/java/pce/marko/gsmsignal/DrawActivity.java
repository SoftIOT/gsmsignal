package pce.marko.gsmsignal;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

/**
 * Created by Davor on 7/14/2017.
 */

public class DrawActivity extends Activity {

    DrawView drawView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        drawView = new DrawView(this);
        drawView.setBackgroundColor(Color.WHITE);

        setContentView(drawView);
        // setContentView(R.layout.activity_draw);

        Intent intent = getIntent();
        Bundle extras = getIntent().getExtras();

        String val1 = intent.getStringExtra("val1");
        String val2 = extras.getString("val2");

        Log.d("drawActivity ", val1 + " : " + val2);
    }


    class DrawView extends View {
        Paint paint = new Paint();

        public DrawView(Context context) {
            super(context);
            paint.setColor(Color.BLUE);
        }
        @Override
        public void onDraw(Canvas canvas) {
            super.onDraw(canvas);
            canvas.drawLine(10, 20, 30, 40, paint);
            canvas.drawLine(20, 10, 50, 20, paint);

        }
    }

}
