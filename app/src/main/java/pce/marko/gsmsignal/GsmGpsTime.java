package pce.marko.gsmsignal;

/**
 * Created by Davor on 7/11/2017.
 */

public class GsmGpsTime {

    int id;
    String gsm;
    double gps; // Latitude, Longitude
    String time;

    public GsmGpsTime(){ }

    public GsmGpsTime(String gsm){
        this.gsm = gsm;
    }

    public GsmGpsTime(String gsm, double gps, String time){  }

    public GsmGpsTime(int id, String gsm, double gps, String time){  }


    public int getId(){
        return id;
    }

    public void setId(int id){
        this.id = id;
    }

    public String getGsm(){
        return gsm;
    }

    public void setGsm(String gsm){
        this.gsm = gsm;
    }
}
