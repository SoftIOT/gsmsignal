package pce.marko.gsmsignal;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;

import android.os.Bundle;
import android.os.Handler;
import android.telephony.PhoneStateListener;
import android.telephony.SignalStrength;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;

import static java.lang.Thread.sleep;

public class MainActivity extends Activity {

    TelephonyManager telephonyManager;
    myPhoneStateListener psListener;
    TextView txtSignalStr;
    Button sqlite, start;

    DatabaseHandler db;

    private boolean flagBlink = true;
    private boolean flagBtn = true;
    private boolean flagStart = true;
    private boolean flagInit = true;

    private Handler handler;
    private Runnable runfun;
    private MyThread my_thread;

    private SimpleDateFormat sdf;
    @Override
    protected void onResume() {
        super.onResume();
        // telephonyManager.listen(psListener, PhoneStateListener.LISTEN_SIGNAL_STRENGTHS);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        db = new DatabaseHandler(this);

        txtSignalStr = (TextView)findViewById(R.id.signalStrength);
        sqlite = (Button)findViewById(R.id.button);
        start = (Button)findViewById(R.id.buttonStart);

        // sdf = new SimpleDateFormat("yyyyMMdd_HHmmss");
        sdf = new SimpleDateFormat("HHmmss");

        psListener = new myPhoneStateListener();
        telephonyManager = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
        // telephonyManager.listen(psListener, PhoneStateListener.LISTEN_SIGNAL_STRENGTHS);
        // telephonyManager.listen(psListener, PhoneStateListener.LISTEN_NONE);

        handler = new Handler(); // Handling handler to main thread message queue
        final MyThread my_thread = new MyThread();

        /* final Handler handler1 = new Handler();
        final Runnable r = new Runnable() {
            public void run() {
                txtV.append(" - Hello World");
                handler1.postDelayed(this, 1000); // ovo je kao neka rekurzija, postDelayed(this...
                                                  // sam sebe poziva, ovaj runnable r, this
            }
        };
        handler1.postDelayed(r, 1000); // Jednom pozoves r, prvi put
        */


        sqlite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                List<String> lista = db.getAllGsm();

                for (String str : lista) {
                    Log.d("GSM ", str);
                }

                Intent txtViewIntent = new Intent(MainActivity.this, MyViewActivity.class);
                startActivity(txtViewIntent);
            }
        });

        start.setOnClickListener(new View.OnClickListener() {
            @Override
           public void onClick(View view) {
                // handler.post(run);
                // handler.postDelayed(run, 1000);

                /* PREPORUKA JE OVAKO RADITI SA THREAD-OM, TJ. OVAKO ISKLJUCIVAT I UKLJUCIVAT SE U RAD THREAD, DA ON
                   CIJELO VRIJEME BUDE UKLJUCEN, PA SA FLAG-OVIMA PALI/GASI POJEDINU RADNJU, A NE SISTEMSKI SA
                   THREAD STOP(), PAUSE(), isINTERUPTED() I SLICNO. */
                if(flagInit == true){
                    // MyThread my_thread = new MyThread(); // NE MOZE OVDJE JER SVAKI SE PUT STVARA NOVI THREAD, I SVAKI NOVI THREAD
                                                            // POKRECE NOVI TIMER, NOVO BROJANJE SLEEP(), I TO RADI PROBLEM
                                                            // DOVOLJNO JE JEDAN PUT STVORITI THREAD I TO GLOBALNI
                    flagInit = false;
                    my_thread.start();
                }

                if(flagBtn == true) {
                    flagBtn = false;
                    flagStart = true;
                    start.setText("Stop");

                    // PRIJAVLJEN NA PhoneStateListener-A, PRATI PROMJENJENE GSM VRIJEDNOSTI
                    telephonyManager.listen(psListener, PhoneStateListener.LISTEN_SIGNAL_STRENGTHS);
                    // thread.start(); // NE MOZE SE OVAKO RESUME, TO SE RADI SA FLAGOVIMA
                    // thread.notify();

                    // thread.isInterrupted();
                    // thread.isAlive()

                    // yourThread.setIsTerminating(true); // tell the thread to stop
                    // yourThread.join(); // wait for the thread to stop
                }
                else{
                    flagBtn = true;
                    flagStart = false;
                    start.setText("Start");

                    // ODJAVLJEN SA PhoneStateListener-A, VISE NE PRATI PROMJENJENE GSM VRIJEDNOSTI
                    telephonyManager.listen(psListener, PhoneStateListener.LISTEN_NONE);

                    // thread.interrupt(); // Moze interrupt, ali ga je tesko resume
                    /* try {
                        thread.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } */
                }


            }
        });

        runfun = new Runnable() {
            @Override
            public void run() {
                // txtSignalStr.setText(txtSignalStr.getText() + "\nTest");
                Log.d("POST_HANDLER", txtSignalStr.getText().toString());
            }
        };


    }



    public class myPhoneStateListener extends PhoneStateListener {
        public int signalStrengthValue;

        public void onSignalStrengthsChanged(SignalStrength signalStrength) {
            super.onSignalStrengthsChanged(signalStrength);

            if (signalStrength.isGsm()) {
                if (signalStrength.getGsmSignalStrength() != 99)
                    signalStrengthValue = signalStrength.getGsmSignalStrength() * 2 - 113;
                else
                    signalStrengthValue = signalStrength.getGsmSignalStrength();
            } else {
                signalStrengthValue = signalStrength.getCdmaDbm();
            }

            String currentDateandTime = sdf.format(new Date());
            // VERZIJA SA REGISTRACIJOM listen I unlisten PhoneStateListener
            txtSignalStr.setText("Signal Strength : " + signalStrengthValue + " - " + currentDateandTime);
            db.addGsm(String.valueOf(signalStrengthValue));

            // VERZIJA SA FLAGOVIMA
            /* if(flagBtn == false) {
                txtSignalStr.setText("Signal Strength : " + signalStrengthValue);
                // db.addGsmGpsTime(new GsmGpsTime(txtSignalStr.getText().toString()));
                // if(flagBtn == true)
                db.addGsm(String.valueOf(signalStrengthValue));
            } */
        }
    }

    // Bolje stavit kao inner thread class jer tako ima pristup globalnim varijablama.
    // Kako napravit pristup varijablama iz CustomThread class!? Mozda interface!?
    // public class MyThread implements Runnable{
    public class MyThread extends Thread{

        @Override
        public void run() {
            try{
                while(true){
                    sleep(500);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if(flagStart == true) {
                                 handler.post(runfun); // IZ OVOG THREAD-A SALJES PODATKE NA GLAVNI UI THREAD
                                if (flagBlink == true) {
                                    flagBlink = false;
                                    // txtSignalStr.setTextColor(Color.parseColor("#123abc"));
                                    txtSignalStr.setTextColor(Color.parseColor("#ff0000"));
                                } else {
                                    flagBlink = true;
                                    txtSignalStr.setTextColor(Color.parseColor("#abc123"));
                                }
                                // handler.post(run);
                            }
                        }
                    });

                    // handler.post(run);
                    // handler.postDelayed(run, 300);
                    // postDelayed(run, 1000); samo se poziva za 1s
                }
            }
            catch (InterruptedException e){
                e.printStackTrace();
            }

        }


    }

}

