package pce.marko.gsmsignal;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * Created by Davor on 7/13/2017.
 */

public class MyViewActivity extends Activity {

    TextView txtV;
    DatabaseHandler db;
    List<String> dblista;
    Button delbtn, drawbtn;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_textview);

        txtV = (TextView) findViewById(R.id.textView);
        txtV.setMovementMethod(new ScrollingMovementMethod());

        delbtn = (Button)findViewById(R.id.buttonDelete);
        drawbtn = (Button)findViewById(R.id.buttondraw);

        db = new DatabaseHandler(MyViewActivity.this);
        dblista = new ArrayList<String>();

        dblista = db.getAllGsm();

          txtV.append("\n");
         for(int i = 0; i<dblista.size(); i++){
            // txtV.setText(String.valueOf(i));
            // txtV.append(String.valueOf(i) + "\n");
             txtV.append(String.valueOf(i) + ". " + dblista.get(i) + "\n");
             // txtV.append(db.ge);
             // txtV.append("\n");
         }

         delbtn.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View view) {
                try {
                    db.deleteAll();

                    // txtV.setText(" ");
                    // txtV.refreshDrawableState();

                    dblista = db.getAllGsm();
                    txtV.setText(" ");
                    for(int i = 0; i<dblista.size(); i++){
                        txtV.append(String.valueOf(i) + ". " + dblista.get(i) + "\n");
                    }

                }
                catch (Exception ex){
                    ex.printStackTrace();
                }
             }
         });

         drawbtn.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View view) {
                 Intent drawIntent = new Intent(MyViewActivity.this, DrawActivity.class);
                 drawIntent.putExtra("val1 ", "asix");
                 drawIntent.putExtra("val2 ", "asiy");
                 startActivity(drawIntent);
             }
         });


    }

    /* @Override
    public void onBackPressed() {
      //  super.onBackPressed();
      //  this.finish();
    } */
}
