package pce.marko.gsmsignal;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Davor on 7/11/2017.
 */

public class DatabaseHandler extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "ggt_db";
    private static final String DATABASE_TABLE = "ggt_table";

    private static final String COLUMN_ID = "id";
    private static final String COLUMN_GSM = "gsm";


    // public DatabaseHandler(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {

        String CREATE_TODO_TABLE = "CREATE TABLE " + DATABASE_TABLE + "(" + COLUMN_ID + " INTEGER PRIMARY KEY," + COLUMN_GSM + " TEXT" + ");";
        // "CREATE TABLE todo_table (id INTEGER PRIMARY KEY NOT NULL, todo TEXT NOT NULL);" // Na kraju obavezno ;
        db.execSQL(CREATE_TODO_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE);

        // Create tables again
        onCreate(db);
    }


    //
    public void addGsm(String gsm){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_GSM, gsm);

        db.insert(DATABASE_TABLE, null, contentValues);
        // db.insertWithOnConflict(DATABASE_TABLE, null, contentValues, null, null, null, null);
        db.close();
    }

    public List<String> getAllGsm(){

        List<String> gsmLista = new ArrayList<String>();
        String upit = "SELECT * FROM " + DATABASE_TABLE;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(upit, null);

        if(cursor.moveToFirst()){
            do {
                // GsmGpsTime todoItem = new GsmGpsTime();
                // todoItem.setId(Integer.parseInt(cursor.getString(0))); // MORAS I id IZVUC IZ TABLICE DA BI TO ZAJEDNO SA STRINGOM MOGAO POKAZAT.
                // todoItem.setGsm(cursor.getString(1));
                String gsmItem = cursor.getString(1);
                gsmLista.add(gsmItem);
                //  todoLista.add(todoItem);
            }while(cursor.moveToNext());
        }

        db.close();
        // return todoLista;
        return gsmLista;
    }


    //

    public void addGsmGpsTime(GsmGpsTime gsmGpsTime){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_GSM, gsmGpsTime.getGsm());

        db.insert(DATABASE_TABLE, null, contentValues);
        // db.insertWithOnConflict(DATABASE_TABLE, null, contentValues, null, null, null, null);
        db.close();
    }


    public List<GsmGpsTime> getAllGsmGpsTime(){

        List<GsmGpsTime> todoLista = new ArrayList<GsmGpsTime>();
        String upit = "SELECT * FROM " + DATABASE_TABLE;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(upit, null);

        if(cursor.moveToFirst()){
            do {
                GsmGpsTime todoItem = new GsmGpsTime();
                todoItem.setId(Integer.parseInt(cursor.getString(0))); // MORAS I id IZVUC IZ TABLICE DA BI TO ZAJEDNO SA STRINGOM MOGAO POKAZAT.
                todoItem.setGsm(cursor.getString(1));
                // String todoItem =
                todoLista.add(todoItem);
            }while(cursor.moveToNext());
        }

        db.close();
        return todoLista;
    }


    public void deleteAll(){
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(DATABASE_TABLE, null, null);
    }

    //  public List<TodoItem> deleteTodo(int id){
    public void deleteTodo(int id){
        // List<TodoItem> retList = new ArrayList<>();
        SQLiteDatabase db = this.getWritableDatabase();

        db.delete(DATABASE_TABLE, COLUMN_ID + " = ?", new String[]{String.valueOf(id)});
        Log.d("DELETE ", String.valueOf(id));

        db.close();

        // return getAllTodos();
    }

    public void updateTodo(GsmGpsTime gsmGpsTime){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues content = new ContentValues();
        content.put(COLUMN_GSM, gsmGpsTime.getGsm());

        Log.d("UPDATE ", String.valueOf(gsmGpsTime.getId()) + " . " + gsmGpsTime.getGsm());

        db.update(DATABASE_TABLE, content, COLUMN_ID + " = ? ", new String[]{String.valueOf(gsmGpsTime.getId())});
    }

    public int getTodoCounts() {
        String countQuery = "SELECT  * FROM " + DATABASE_TABLE;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        cursor.close();

        // return count
        return cursor.getCount();
        /* String countTodo = "SELECT * FROM " + DATABASE_TABLE;
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cur = db.rawQuery(countTodo, null);
        int result = cur.getCount();

        cur.close();
        db.close();
        return result; */
    }

}
